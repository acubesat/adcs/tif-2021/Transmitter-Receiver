    #include <SPI.h>
    #include <nRF24L01.h>
    #include <RF24.h>
    RF24 radio(9, 10); // CE, CSN
    const byte addresses [][6] = {"00001", "00002"};  //Setting the two addresses. One for transmitting and one for receiving

    float roll;
    int timoniPin = A0;

    void setup() {

      Serial.begin(9600);

      radio.begin();                           //Starting the radio communication
      radio.openWritingPipe(addresses[1]);     //Setting the address at which we will send the data
      radio.openReadingPipe(1, addresses[0]);  //Setting the address at which we will receive the data
      radio.setPALevel(RF24_PA_MAX); //You can set it as minimum or maximum depending on the distance between the transmitter and receiver. 
    }
    void loop() 
    {  
      int timoniValue = analogRead(timoniPin);
//      int timoniInput = 0;
//      if (timoniValue<260){
//        timoniInput = map(timoniValue,0,260,-130,0);
//      }
//      else if(timoniValue>=260){
//        timoniInput = map(timoniValue,260,812,0,130);
//      }
      int timoniInput = map(timoniValue,0,890,-130,130);
    //  int data_to_be_sent[] = {timoniInput}; 
      
//      Serial.println(timoniValue);
      //Serial.print(", ");
      delay(5);
      radio.stopListening();                             //This sets the module as transmitter
      radio.write(&timoniValue, sizeof(timoniValue));  //Sending the data
      delay(5);
//      Serial.println("sent");
      radio.startListening();                            //This sets the module as receiver
      if (radio.available())                     //Looking for incoming data
       {
        radio.read(&roll, sizeof(roll));
//        Serial.println(roll);
        }
      //while(!radio.available());                         //Looking for incoming data
      //radio.read(&button_state1, sizeof(button_state1)); //Reading the data
      //Serial.println(button_state1);
//      Serial.print("Timoni = "); Serial.print(timoniInput); 
//      Serial.print(" Roll = "); Serial.println(roll); 
Serial.print(roll);
Serial.print(", "); 
    }
